public class Main {
    public static void main(String[] args) {
        init();
    }

    public static void init() {
        Classwork classwork = new Classwork();
        classwork.bufferMax();
        classwork.cycle();
    }
}