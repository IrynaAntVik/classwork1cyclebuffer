import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Classwork {

    private BufferedReader reader;

    public void bufferMax(){
        reader = new BufferedReader(new InputStreamReader(System.in));
        int numberX;
        int numberY;

        try {
            System.out.println("Введите 1-е значение");
            numberX = Integer.parseInt(reader.readLine());
            for (int i = 2; i <= 4; i++) {
                System.out.println("Введите" + i + "-е значение");
                numberY = Integer.parseInt(reader.readLine());
                if (numberX > numberY) {
                    numberX = numberX;
                } else {
                    numberX = numberY;
                }
            }
            System.out.println("Максимальное из введенных значений");
            System.out.println(numberX);
        }
        catch (NumberFormatException n){
        System.out.println("Все введенные значения должны быть числовыми");
        bufferMax();
        }
        catch (IOException e) {
        e.printStackTrace();
        }
    }

    /*public int help(int x, int y) {
        if (x > y) {
            return x;
        } else {
            return y;
        }
    }

    public int max(int numberA, int numberB, int numberC, int numberD) {
        return help(help(numberA, numberB), help(numberC, numberD));
    }*/

    public void cycle() {
        System.out.println("В цикле вывести в консоль от A до Z");
        for (char i = 'A'; i <= 'Z'; i++) {
            System.out.print(i + " ");
        }
        System.out.println("" + "\n" +"В цикле вывести в консоль от Z до A");
        for (char i = 'Z'; i >= 'A'; i--) {
            System.out.print(i + " ");
        }
        System.out.println("" + "\n" +"В цикле вывести в консоль от А до Я");
        for (char i = 'А'; i <= 'Я'; i++) {
            System.out.print(i + " ");
        }

        System.out.println("" + "\n" +"В цикле вывести в консоль от я до б");
        for (char i = 'я'; i >= 'б'; i--) {
            System.out.print(i + " ");
        }
        System.out.println("" + "\n" +"В цикле вывести в консоль от 0 до 90");
        for (int i = 0; i <= 90; i++) {
            System.out.print(i + " ");
        }
        System.out.println("" + "\n" +"В цикле вывести в консоль от 90 до 0");
        for (int i = 90; i >= 0; i--) {
            System.out.print(i + " ");
        }
        System.out.println("" + "\n" +"В цикле вывести в консоль от -10 до 20");
        for (int i = -10; i <= 20; i++) {
            System.out.print(i + " ");
        }
        System.out.println("" + "\n" +"while index от 0 до 20");
        int index = 0;
        while (index >= 0 && index <= 20) {
            System.out.print(index + " ");
            index++;
        }
        System.out.println("" + "\n" +"В цикле do while вывести");
        do {
            index += 0;
        } while (index == 25);
        System.out.print("do while index = " + index);
        System.out.println("" + "\n" +"Квадрат");
        int n = 5;
        for (int i = 1; i <= n; i++) {
            for (int j = 1; j <= n; j++) {
                if (i == 1 || i == n) {
                    System.out.print("* ");
                } else if ( j == 1 || j == n) {
                    System.out.print("* ");
                } else {
                    System.out.print("  ");
                }
            }
            System.out.println("");
        }
    }
}
